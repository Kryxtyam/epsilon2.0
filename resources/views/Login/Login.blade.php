<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{asset('assest/images/favicon.png')}}">

    <title>Epsilon V2.0</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet'
          type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assest/js/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('assest/fonts/font-awesome-4/css/font-awesome.min.css')}}">

    <!-- Custom styles for this template -->
    <link href="{{asset('assest/css/style.css')}}" rel="stylesheet"/>

</head>

<body class="texture">

<div id="cl-wrapper" class="login-container">
    {{--<form action="validatelogin" method="">--}}
    <div class="middle-login">
        <div class="block-flat">
            <div class="header">
                <h3 class="text-center"><img class="logo-img" src="{{asset('assest/images/logo.png')}}" alt="logo"/>Epsilon
                </h3>
            </div>
            <div>
                {{--{{Form::open(array('onsubmit'=>'return false','id'=>'Login'))}}--}}
                {{--<form style="margin-bottom: 0px !important;" class="form-horizontal" id="Login">--}}
                <form class="form-horizontal" role="form" method="POST" action="Authentication">
                    <div class="content">
                        <h4 class="title">Acceso a epsilon</h4>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" placeholder="Usuario" id="idusuario" name="idusuario"
                                           class="form-control"
                                           required
                                           data-validation-required-message=
                                           "Debe de ingresar el usuario">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <input type="password" placeholder="Contraseņa" id="password" name="password"
                                           class="form-control"
                                           required data-validation-required-message=
                                           "Debe de ingresar la contraseņa">
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="foot">
                        <input type="hidden" value="{{csrf_token()}}" name="_token">
                        <button type="submit" class="btn btn-primary">Entrar</button>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="error">
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            Por favor corrige los siguientes errores:<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="text-center out-links"><a href="http://www.prodiagnostico.com">&copy; 2015 Prodiagnostico S.A</a>
        </div>
    </div>

</div>
<script type="text/javascript" src="{{asset('assest/js/ajax.js')}}"></script>
<script src="{{asset('assest/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/behaviour/general.js')}}"></script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{asset('assest/js/behaviour/voice-commands.js')}}"></script>
<script src="{{asset('assest/js/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.flot/jquery.flot.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.flot/jquery.flot.pie.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.flot/jquery.flot.resize.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.flot/jquery.flot.labels.js')}}"></script>

</body>
</html>