<script src="{{asset('assest/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.nanoscroller/jquery.nanoscroller.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.sparkline/jquery.sparkline.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.easypiechart/jquery.easy-pie-chart.js')}}"></script>
<script src="{{asset('assest/js/ajax.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
<script src="{{asset('assest/js/jquery.parsley/parsley.js')}}" type="text/javascript"></script>
<script src="{{asset('assest/js/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('assest/js/ckeditor/adapters/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/bootstrap.summernote/dist/summernote.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/bootstrap.wysihtml5/src/bootstrap-wysihtml5.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/bootstrap.switch/bootstrap-switch.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/behaviour/general.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.ui/jquery-ui.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.nestable/jquery.nestable.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/bootstrap.switch/bootstrap-switch.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('assest/js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/bootstrap.slider/js/bootstrap-slider.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.gritter/js/jquery.gritter.js')}}"></script>
<!--<script type="text/javascript" src="../js/jquery.niftymodals/js/jquery.modalEffects.js"></script> -->
<script type="text/javascript" src="{{asset('assest/js/jquery.datatables/jquery.datatables.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('assest/js/jquery.datatables/bootstrap-adapter/js/datatables.js')}}"></script>
<script type="text/javascript">
    <!--
    //initialize the javascript
    //App.init();
    //$('.md-trigger').modalEffects();
    //});-->
    <!-- funciona para datetables-->
    //Add dataTable Functions
    $(document).ready(function () {
        //initialize the javascript
        App.init();
    });
</script>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{asset('assest/js/behaviour/voice-commands.js')}}"></script>
<script src="{{asset('assest/js/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.flot/jquery.flot.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.flot/jquery.flot.pie.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.flot/jquery.flot.resize.js')}}"></script>
<script type="text/javascript" src="{{asset('assest/js/jquery.flot/jquery.flot.labels.js')}}"></script>