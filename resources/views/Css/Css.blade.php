<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href={{asset('assest/images/favicon.png')}}">
<title>Epsilon V2.0 - Prodiagnostico S.A</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700,800' rel='stylesheet'
      type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:300,200,100' rel='stylesheet' type='text/css'>
<!-- Bootstrap core CSS -->
<link href="{{asset('assest/js/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('assest/js/jquery.gritter/css/jquery.gritter.css')}}"/>
<link rel="stylesheet" href="{{asset('assest/fonts/font-awesome-4/css/font-awesome.min.css')}}">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="{{asset('assets/js/html5shiv.js')}}"></script>
<script src="{{asset('assets/js/respond.min.js')}}"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="{{asset('assest/js/jquery.nanoscroller/nanoscroller.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assest/js/jquery.easypiechart/jquery.easy-pie-chart.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assest/js/bootstrap.switch/bootstrap-switch.css')}}"/>
<link rel="stylesheet" type="text/css"
      href="{{asset('assest/js/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assest/js/jquery.select2/select2.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assest/js/bootstrap.slider/css/slider.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assest/js/bootstrap.wysihtml5/src/bootstrap-wysihtml5.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assest/js/bootstrap.summernote/dist/summernote.css')}}"/>
<!--<link rel="stylesheet" type="text/css" href="../js/jquery.datatables/bootstrap-adapter/css/datatables.css" />-->
<link rel="stylesheet" type="text/css" href="{{asset('assest/js/jquery.niftymodals/css/component.css')}}"/>
<!-- Custom styles for this template -->
<link href="{{asset('assest/css/style.css')}}" rel="stylesheet"/>
