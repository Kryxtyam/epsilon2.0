<!DOCTYPE html>
<html lang="en">
<head>
    <tittle>@yield('tittle')</tittle>
    @include('Css.Css')
</head>
<body>
<!-- Menu superior y lateral - Incluir Siempre -->
{{--//Barra de menu superior--}}
@include('Menu.top_menu')
{{--//barra de menu lateral izquierda--}}
<div id="cl-wrapper">
    @include('Menu.left_menu')
            <!-- Area de trabajo - Realizar aqui codigo fuente -->
    @yield('content')
    <form method="post" id="refreshrequest" action="refreshrequest">
        <input type="hidden" name="_method" value="POST">
        <input type="hidden" name="area" id="area" value="{{$Data->idarea}}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>
</div>
@include('Js.Javascript');
</body>
</html>