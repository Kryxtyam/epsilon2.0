//AJAX
//---------------------------------------------------------------------------------------------
function nuevoAjax() {
    /* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
     lo que se puede copiar tal como esta aqui */
    var xmlhttp = false;
    try {
        // Creacion del objeto AJAX para navegadores no IE
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");

    }
    catch (e) {
        try {
            // Creacion del objet AJAX para IE
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

        }
        catch (E) {

            if (!xmlhttp && typeof XMLHttpRequest != 'undefined') xmlhttp = new XMLHttpRequest();
        }
    }
    return xmlhttp;
}
function ajaxFunction() {
    var xmlHttp;

    try {

        xmlHttp = new XMLHttpRequest();
        return xmlHttp;
    } catch (e) {

        try {
            xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
            return xmlHttp;
        } catch (e) {

            try {
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                return xmlHttp;
            } catch (e) {
                alert("Tu navegador no soporta AJAX!");
                return false;
            }
        }
    }
}
var xmlhttp = false;

//Chequeo si se usa IExplorer.
try {
    //Si la version de Javascript es mayor que la 5.
    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
} catch (e) {

    //If not, then use the older active x object.
    try {
        //If we are using Internet Explorer.
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (E) {
        //Si no, no se esta usando un Internet Explorer.
        xmlhttp = false;
    }
}
//Si no se esta usando un IExplorer, se crea una instancia javascript del objeto.
if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    xmlhttp = new XMLHttpRequest();
}
//+++++++++++++++++++++++++++++++++++++++++++Validacion usuario existe en base de datos.++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function ValidarUsuario() {
    var username, password;
    username = document.getElementById('username').value;
    password = document.getElementById('password').value;

    //Codigo ajax para enviar datos al servidor y obtener respuesta
    error = document.getElementById('error');//etiqueta donde se va a mostrar la respuesta
    ajax = nuevoAjax();
    //llamado al archivo que va a ejecutar la consulta ajax
    ajax.open("POST", "validate/validateLogin.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState == 4) {
            if (ajax.responseText == 'false') {
                error.innerHTML = '<div class="alert alert-danger alert-white rounded"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><div class="icon"><i class="fa fa-times-circle"></i></div><center><font color="red" size="2.5">El usuario o la contraseña no son validos.<font></center></div>';
            }
            else {
                window.location.href = "Contenido/home.php"
            }
        }
    }
    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    ajax.send("username=" + username + "&password=" + password + "&tiempo=" + new Date().getTime());
}
//********************************** funcion para actualizar los pendientes de solicitudes - actualizar cada 10 seg *********************************************************//
function RefreshContador(usuario) {
    //Codigo ajax para enviar datos al servidor y obtener respuesta


    contador = document.getElementById('contador');//etiqueta donde se va a mostrar la respuesta
    ajax = nuevoAjax();
    //llamado al archivo que va a ejecutar la consulta ajax
    ajax.open("POST", "../ContadorSolicitudes/QuerySolicitudes.php", true);
    ajax.onreadystatechange = function () {
        if (ajax.readyState == 4) {
            contador.innerHTML = ajax.responseText;
        }
    }
    ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    ajax.send("area=" + area + "&usuario=" + usuario + "&tiempo=" + new Date().getTime());
}
//******************************** funciona para actualizar las notificaciones - actualizar cada 10 seg ***************************************//
function RefreshNotificacion(area, usuario) {
    //Codigo ajax para enviar datos al servidor y obtener respuesta
    notificaciones = document.getElementById('notificaciones');//etiqueta donde se va a mostrar la respuesta
    ajax2 = nuevoAjax();
    //llamado al archivo que va a ejecutar la consulta ajax
    ajax2.open("POST", "../ContadorSolicitudes/QueryNotificaciones.php", true);
    ajax2.onreadystatechange = function () {
        if (ajax2.readyState == 4) {
            notificaciones.innerHTML = ajax2.responseText;
        }
    }
    ajax2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    ajax2.send("area=" + area + "&usuario=" + usuario + "&tiempo=" + new Date().getTime());
}
//************************** deshabilitar el boton atras del navegador ***********************************************//
function nobackbutton() {
    window.location.hash = "no-back-button";
    window.location.hash = "Again-No-back-button" //chrome
    window.onhashchange = function () {
        window.location.hash = "no-back-button";
    }
}
function RefresDiv() {
    var form, url, data, html;
    form = $('#refreshrequest');
    url = form.attr('action');
    data = form.serialize();
    html = '';
    $.post(url, data, function (result) {
        $('#cantidadsolicitudes').html(result.cantidad);
        $.each(result.notificacion, function (index, value) {
            html += '<li><a href="#"><i class="fa fa-male info"></i><b>' +
                '<strong>' + value.nombre + '</strong>' +
                '</b> Realizó una solicitud' +
                ' <span class="date">' + value.fecha + ' ' + value.hora + '</span></a></li>'

        });
        $('#solpendientes').html(html);
    });

}
$(document).ready(function () {
    RefresDiv();
});
//setInterval(RefresDiv, 10000000);