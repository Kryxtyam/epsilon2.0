<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    protected $table = 'solicitud';
    protected $primaryKey = 'idsolicitud';
    public $timestamps = false;

    public function Funcionario()
    {
        return $this->belongsTo('App\Models\Funcionario', 'idfuncionario');
    }
}
