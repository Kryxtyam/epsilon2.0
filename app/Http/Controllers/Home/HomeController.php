<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Funcionario;
use App\Models\Solicitud;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $FuncionarioSession = Funcionario::find(Auth::user()->idusuario);
        $nombrear = explode(' ', $FuncionarioSession->nombres);
        $nombre = $nombrear[0];
        $FuncionarioSession->nombres = $nombre;
        $apellidosar = explode(' ', $FuncionarioSession->apellidos);
        $apellido = $apellidosar[0];
        $FuncionarioSession->apellidos = $apellido;
        return view('Home.Home')->with('Data', $FuncionarioSession);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $area = $request->input('area');
        $solicitudes = Solicitud::whereRaw('idarea = ? and idestado_solicitud=1', [$area])->get();
        $notificaciones = array();
        foreach ($solicitudes as $soli) {
            $nombre = ucfirst(strtolower($soli->Funcionario->nombres));
            $fecha = $soli->fechahora_solicitud;
            $hora = $soli->horasolicitud;
            $notificaciones[] = array('nombre' => $nombre, 'fecha' => $fecha, 'hora' => $hora);
        }
        $cantidad = $solicitudes->count();
        $json = array();
        $json['cantidad'] = $cantidad;
        $json['notificacion'] = $notificaciones;
        if ($request->ajax()) {
            return response()->json(
                $json
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
