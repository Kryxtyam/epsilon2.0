<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Direccionamiento a la pagina de Login
Route::get('Login', 'Auth\AuthController@getLogin');

//Direccionamiento a Controller para validar los datos ingreados en la pantalla de login
Route::post('Authentication', 'Auth\AuthController@postLogin');

//Direccionamiento a la pagina de bienvenida
Route::get('Home','Home\HomeController@index');

Route::post('refreshrequest','Home\HomeController@store');

//Route::get('Home', function () {
//    return view('Home.Home');
//}
//);

//Ruta para destruir la session y salir al login
Route::get('LogOut', 'Auth\AuthController@getLogout');



